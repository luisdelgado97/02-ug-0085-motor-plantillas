const express = require('express');
const hbs = require('hbs');
const app = express();
const db = require( './db/data' );

//creacion de aplicacion express
app.use(express.static('public'));
app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');

hbs.registerPartials(__dirname + "/views/partials");

//correccion de rutas
app.get("/", (request, response) => {
    const matriculaDeHome = "Home";
    const Home = [];
    for (indice in db.media) {
        if (matriculaDeHome === db.media[indice].matricula)
            Home.push(db.media[indice]);
    }
    console.log("DatosDeHome", Home);
    response.render("index", {
        data: Home,
        base_url: "http://localhost:8080",
        integrantes: db.integrantes
    });
});
app.get("/integrantes/:matricula", (request, response) => {
    const matricula = request.params.matricula;

    let integrante = {};
    for (let indice in db.integrantes) {
        if (matricula === db.integrantes[indice].matricula) {
            integrante = db.integrantes[indice];
        }
    }
    const datos = [];
    for (let indice in db.media) {
        if (matricula === db.media[indice].matricula) {
            datos.push(db.media[indice]);
        }
    }

    if (Object.keys(integrante).length === 0) {
        response.status(404).render("error");
    } else {
        response.render("integrante", {
            data: datos,
            base_url: "http://localhost:8080",
            integrantes: db.integrantes,
        });
    }
});

app.get("/paginas/word_cloud.html", (request, response) => {
    response.render("word_cloud",{
        integrantes: db.integrantes,
        base_url: "http://localhost:8080",
    });
});
app.get("/paginas/curso.html", (request, response) => {
    response.render("curso",{
        integrantes: db.integrantes,
        base_url: "http://localhost:8080",
    });
    
});

app.use ((request, response, next)=>{
    response.status(404).render ('error');
});


app.listen(8080, () => {
    console.log("El servidor se está ejecutando en http://localhost:8080");

});


